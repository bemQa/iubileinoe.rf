$(document).ready(function () {

    $(".main-slider-init").on('init', function (e) {
        if ($('.main-slide').length === 1) {
            $('.main-slider-init .slick-dots').addClass('hidden')
        }
    });

    $(".main-slider-init").slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: false,
        nextArrow: false,
        autoplay: true,
        autoplaySpeed: 5000
    })

    

    $(".product-slider-init").slick({
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: false,
        nextArrow: false,
        autoplay: true,
        autoplaySpeed: 4000,
        infinite: true,
        dots: false,
        responsive: [{
            breakpoint: 1000,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
            }
        }]
    });

    $('.burger').click(function (e) {
        $(this).toggleClass('active');
        $('.logo').toggleClass('active');
        $('.menu').toggleClass('active');
        $('.desktop-lk').fadeToggle(300);

        $('body').toggleClass('mob_scroll_false');
    })

    $('.close').click(function (e) {
        $('.burger, .logo, .menu, .desktop-lk').removeClass('active');
        $('.desktop-lk').fadeToggle(300);
        $('body').removeClass('mob_scroll_false');
    })

    // if(window.innerWidth > 1000) {
    // 	$('.burger, .logo, .menu').addClass('active');
    // 	$('body').addClass('mob_scroll_false');
    // }


    if ($(window).width() < 1000){
        $('.footer-bot-left').append($('.socials.socials_footer'))
    }

    $('.accordion__title').click(function (e) {
        $(this).parents('.accordion__item').toggleClass('active')
    })

    $('.lk-tooltip').click(function(e) {
    	e.preventDefault();
    	if(window.innerWidth > 1000) {
    		$('.tooltip-menu').fadeToggle('slow');
		    $('body').on('click', function (e) {
		      var div = $('.menu__ll-lk');

		      if (!div.is(e.target) && div.has(e.target).length === 0) {
		        $('.tooltip-menu').fadeOut('slow');
		      }
		    });
    	} else {
    		$('.tooltip-menu').toggleClass('active');
    		$('.mob-back').click(function(e) {
				e.preventDefault();
				$('.tooltip-menu').removeClass('active');
    		});
    	}
    });

    function OpenPopup(popupId) {
        $('body').removeClass('no-scrolling');
        $('.popup').removeClass('js-popup-show');
        popupId = '#' + popupId;
        $(popupId).addClass('js-popup-show');
        
        if(!$('.popup').hasClass('popup--map')){
            $('body').addClass('no-scrolling');
        }
    }

    $('.pop-op').click(function (e) {
        e.preventDefault();
        let data = $(this).data('popup');
        OpenPopup(data);
    });

    function closePopup() {
        $('.js-close-popup').on('click', function (e) {
            e.preventDefault();
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scrolling');
        });
    }
    closePopup();

    $('.slider-navigator').slick({
        slidesToShow: 12,
        slidesToScroll: 12,
        asNavFor: '.slider-history',
        dots: false,
        arrows: false,
        focusOnSelect: true,
        infinite: false,
        swipe: false,
        responsive: [{
            breakpoint: 1000,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                swipeToSlide: true,
                swipe: true,
                arrows: true,
                centerMode: true
            }
        }]
    });    
    $('.slider-history').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-navigator',
        prevArrow: '<button type="button" class="slick-prev">назад</button>',
        nextArrow: '<button type="button" class="slick-next">дальше</button>',
        infinite: false,
        swipe: false,
        responsive: [{
            breakpoint: 1000,
            settings: {
                swipe: true,
                adaptiveHeight: true
            }
        }]
    });
    $('.history-btn').click(function(e){
        e.preventDefault();
        $('.slider-history').slick('slickGoTo', 1);
    });
    if(window.innerWidth < 1000) {
        $('.slider-navigator, .slider-history .slick-arrow').css({
            'opacity': 0,
            'height': 0
        });
        // $('.slider-history .slick-arrow').addClass('slick-disabled');
        $('.slider-history').on('afterChange', function(event, slick, currentSlide, nextSlide){
            $('.slider-navigator, .slider-history .slick-arrow').css({
                'opacity': 1,
                'height': ''
            });
            // $('.slider-history .slick-arrow').removeClass('slick-disabled');
            if($('.slick-active').hasClass('start-history')) {
                $('.slider-navigator, .slider-history .slick-arrow').css({
                    'opacity': 0,
                    'height': 0
                });
                // $('.slider-history .slick-arrow').addClass('slick-disabled');
            }
        });
    } else {
        $('.slick-next').text('начать');
        $('.slider-history').on('afterChange', function(event, slick, currentSlide, nextSlide){
            $('.slick-next').text('начать');
            if(!$('.slick-active').hasClass('start-history')) {
               $('.slick-next').text('дальше');
            }
        });
    }

    $('.slider-pagination').slick({
        slidesToShow: 24,
        slidesToScroll: 24,
        dots: false,
        arrows: false,
        focusOnSelect: true,
        infinite: false,
        swipe: false,
        responsive: [{
            breakpoint: 1000,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                swipeToSlide: true,
                swipe: true,
                arrows: true,
                centerMode: true
            }
        }]
    });
    if(window.innerWidth < 1000) {
        var recipe_number = $('.pagination-number.active').data('number');
        $('.slider-pagination').slick('slickGoTo',recipe_number);
    }

    function openAccordion() {
        var accordion = $('.product-more');

        accordion.on('click', function () {
          var $this = $(this);
          var $parent = $(this).parent();
          var content = $parent.next();

          if (content.is(':visible')) {
            $this.text('Развернуть');
            $this.removeClass('active');
            $parent.removeClass('active');
            content.slideUp('fast');
          } else {
            $this.text('Свернуть');
            $this.addClass('active');
            $parent.addClass('active');
            content.slideDown('fast');
          }

        });
    }
    openAccordion();

    $('.tab-trigger').click(function(){
        var $parent = $(this).parent();
        var $item_parent = $(this).parent().next();
        var tab = $(this).data('tab');

        $parent.find('.tab-trigger').removeClass('active');
        $(this).addClass('active');
        $item_parent.find('.tab-item').removeClass('active');
        $item_parent.find('.tab-item.' + tab).addClass('active');
    });

    $('.buy2-link-click').click(function(e) {
        e.preventDefault();
        $(this).siblings('.buy2-link-tooltip').fadeIn();
    });

    $('.close-buy2-link-tooltip').click(function(e) {
        e.preventDefault();
        $(this).parent().fadeOut();
    });
});

( function() {

    var youtube = document.querySelectorAll( ".youtube" );
    
    for (var i = 0; i < youtube.length; i++) {
        
        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
        
        var image = new Image();
            image.src = source;
            image.addEventListener( "load", function() {
                youtube[ i ].appendChild( image );
            }( i ) );
    
            youtube[i].addEventListener( "click", function() {

                var iframe = document.createElement( "iframe" );

                    iframe.setAttribute( "frameborder", "0" );
                    iframe.setAttribute( "allowfullscreen", "" );
                    iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1&enablejsapi=1" );

                    this.innerHTML = "";
                    this.appendChild( iframe );
            } );    
    };
    
} )();

$(window).on('load', function() {
    setTimeout(function(){
        $('.preloader').fadeOut();
    },500);
});